<?php

use Drupal\Core\Url;

/**
 * Implements hook_requirements().
 */
function ossfs_requirements($phase) {
  $requirements = [];
  if ($phase == 'install') {
    if (!class_exists('\OSS\OssClient')) {
      $requirements['ossfs_sdk'] = [
        'description' => t('Ossfs require Aliyun OSS php sdk.'),
        'severity' => REQUIREMENT_ERROR,
      ];
    }
  }
  if ($phase == 'runtime') {
    $config = \Drupal::config('ossfs.settings');
    // Check OSS credential.
    if (!$config->get('access_key') || !$config->get('secret_key')) {
      $requirements['ossfs_config'] = [
        'title' => t('OSS File System'),
        'severity' => REQUIREMENT_ERROR,
        'value' => t('Missing credential'),
        'description' => t('OSS File System access key ID or access key secret is not set. Please set it up at the <a href=":settings">OSS File System module settings page</a>.', [
          ':settings' => Url::fromRoute('ossfs.admin_settings')->toString(),
        ]),
      ];
    }
    elseif (\Drupal::moduleHandler()->moduleExists('image')) {
      // Check style names mapping.
      $styles = $config->get('styles');
      $storage = \Drupal::service('entity_type.manager')->getStorage('image_style');
      $names = $storage->getQuery()->execute();
      foreach ($names as $name) {
        if (empty($styles[$name])) {
          $requirements['ossfs_config'] = [
            'title' => t('OSS File System'),
            'severity' => REQUIREMENT_ERROR,
            'value' => t('Not completed'),
            'description' => t('Style names mapping is not completed. Please set it up at the <a href=":settings">OSS File System module settings page</a>.', [
              ':settings' => Url::fromRoute('ossfs.admin_settings')->toString(),
            ]),
          ];
          break;
        }
      }
    }

    if (ini_get('allow_url_fopen')) {
      $requirements['ossfs_allow_url_fopen'] = [
        'severity' => REQUIREMENT_OK,
        'title' => t('allow_url_fopen'),
        'value' => t('Enabled'),
      ];
    }
    else {
      $requirements['ossfs_allow_url_fopen'] = [
        'severity' => REQUIREMENT_ERROR,
        'title' => t('allow_url_fopen'),
        'value' => t('Disabled'),
        'description' => t('The OSS File System module requires that the allow_url_fopen setting be turned on in php.ini.'),
      ];
    }

    if (PHP_INT_SIZE === 8) {
      $requirements['ossfs_int64'] = [
        'title' => t('PHP architecture'),
        'value' => t('64-bit'),
        'severity' => REQUIREMENT_OK,
      ];
    }
    else {
      $requirements['ossfs_int64'] = [
        'title' => t('PHP architecture'),
        'value' => t('32-bit'),
        'description' => t('A 64-bit PHP installation is required in order to support files larger than 2GB.'),
        'severity' => REQUIREMENT_WARNING,
      ];
    }

  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function ossfs_schema() {
  $schema['ossfs_file'] = [
    'description' => 'Stores metadata about files in OSS.',
    'fields' => [
      'uri' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'binary' => TRUE,
        'description' => 'The OSS URI of the file.',
      ],
      'type' => [
        'type' => 'varchar_ascii',
        'length' => 32,
        'not null' => TRUE,
        'description' => 'The type of the file. Possible values are dir, link, file.',
      ],
      'filemime' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'The file mime type.',
      ],
      'filesize' => [
        'type' => 'int',
        'size' => 'big',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The size of the file in bytes.',
      ],
      'imagesize' => [
        'type' => 'varchar_ascii',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The size of an image.',
      ],
      'changed' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The timestamp that the file was last changed.',
      ],
    ],
    'indexes' => [
      'changed' => ['changed'],
    ],
    // Primary key is case sensitive, see the 'binary' setting.
    'primary key' => [['uri', 191]],
  ];

  return $schema;
}
